function [y,Fs]=midi2synth(input,Fs,synthtype,channel)
%% Tato funkce vychazi z pozmenene funkce "midi2audio" navrzene panem 
% Ken Schutte. Typ nastroje pro polyfonii je nacten primo z MIDI souboru:
% y = midi2audio(input, Fs)
% pripadne jej lze zadat pomoci argumentu synthtype:
% y = midi2audio(input, Fs, synthtype)
% Informace o typu nastroje se predava v devatem sloupci promenne NOTES a 
% odpovida cislu nastroje podle standardu MIDI (viz. help.pdf).
% 
% Pomoci argumentu channel muzete nastavit take cislo kanalu, coz muze byt
% uzitecne jelikoz kanal 10 je vyhrazen perkusnim zvukum a ridi se vlastni
% tabulkou nastroju.
% y = midi2audio(input, Fs, synthtype, channel)
%
% Convert midi structure to a digital waveform
%
% Inputs:
%  input - can be one of:
%    a structure: matlab midi structure (created by readmidi.m)
%    a string: a midi filename
%    other: a 'Notes' matrix (as ouput by midiInfo.m)
%
%  synthtype - string to choose synthesis method
%      passed to synth function in synth.m
%      current choices are: 'fm', 'sine' or 'saw'
%      default='fm'
%
%  Fs - sampling frequency in Hz (beware of aliasing!)
%       default =  44.1e3

% Copyright (c) 2009 Ken Schutte
% more info at: http://www.kenschutte.com/midi

if (nargin<2)
  Fs=48e3;
end

endtime = -1;
if (isstruct(input))
  [Notes,endtime] = midiInfo_polyphony(input, 0);
  Notes(:,2)=Notes(:,2)+1; % korekce - puvodni kod pocita kanaly a instrumenty od nuly
  Notes(:,9)=Notes(:,9)+1;
elseif (ischar(input))
  [Notes,endtime] = midiInfo_polyphony(readmidi(input), 0);
  Notes(:,2)=Notes(:,2)+1; % korekce - puvodni kod pocita kanaly od nuly
  Notes(:,9)=Notes(:,9)+1;
else
  Notes = input;
end

if (nargin>2)
  Notes(:,9)=synthtype;
  disp('=======================================================')
  disp(['All channels will be synthesized with intrument no. ' num2str(synthtype)])
end

if (nargin==4)
  Notes(:,2)=channel;
  disp(['All channels will be synthesized as channel no. ' num2str(channel)])
end

% t2 = 6th col
if (endtime == -1)
    
  endtime = max(Notes(:,6));
end
if (length(endtime)>1)
  endtime = max(endtime);
end

y = zeros(1,ceil(endtime*Fs));

for i=1:size(Notes,1)

  f = midi2freq(Notes(i,3));
  dur = Notes(i,6) - Notes(i,5);
  amp = Notes(i,4)/127;
  synthtype = Notes(i,9);
  channel = Notes(i,2);
  yt = synth(f, dur, amp, Fs, synthtype, channel);

  n1 = floor(Notes(i,5)*Fs)+1;
  N = length(yt);  

  n2 = n1 + N - 1;
  
  % hack: for some examples (6246525.midi), one yt
  %       extended past endtime (just by one sample in this case)
  % todo: check why that was the case.  For now, just truncate,
  if (n2 > length(y))
    ndiff = n2 - length(y);
    % 
    yt = yt(1:(end-ndiff));
    n2 = n2 - ndiff;
  end

  % ensure yt is [1,N]:
  y(n1:n2) = y(n1:n2) + reshape(yt,1,[]);

end
disp('=======================================================')
disp('*** It''s done! Enjoy the music :-) ***')
