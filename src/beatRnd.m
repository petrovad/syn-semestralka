function pattern = beatRnd(p_kick, p_snare, p_hat)

barNum = 1;
barLen = 16;

patternLen = barNum * barLen;
pattern = zeros(patternLen, 3);

for ii = 1 : patternLen
    % kick
    if (mod(ii, barLen * 2) == 1) % first beat of the bar
        pattern(ii, 1) = 127;
    else
        pattern(ii, 1) = (rand() < p_kick) * randVelocity();
    end;

    % snare
    if (pattern(ii, 1) < 1)
        pattern(ii, 2) = (rand() < p_kick) * randVelocity();
    else 
        if (ii ~= patternLen)
            pattern(ii + 1, 2) = (rand() < p_snare) * randVelocity();
        end;
    end;
    
    % hihat
    pattern(ii, 3) = (rand() < p_hat) * randVelocity();
end;
