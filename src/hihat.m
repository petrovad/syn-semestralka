function y = hihat(note, fs, open)

% definitions
f1 = getFreq(note, 5);

if (open)
    % open
    toneLen = 1;
    tau = 0.25;
    tau_n = 0.125;
    t = 0 : 1/fs : toneLen - (1/fs);
    env = exp(-t/tau);
    env_n = exp(-t/tau_n);
    
    reverbname = './res/IMreverbs/Cement Blocks 2.wav';
else
    % closed
    toneLen = 0.2;
    tau = 0.05;
    t = 0 : 1/fs : toneLen - (1/fs);
    env = exp(-t/tau);
    env_n = env;
    
    reverbname = './res/IMreverbs/Cement Blocks 1.wav';
end;

% synthesis
y = conv(noisewave(toneLen, fs), square(2 * pi * f1 * t));

y = y(1 : length(t));
y = y(end : -1 : 1);

% mastering
y = mix(y .* env, noisewave(toneLen, fs) .* env_n, 1, 1);
y = filter_wrap(f1, fs, y, 'high');
y = reverb(y, reverbname);
y = compress(y, 0.7, 1000);
y = normalize(y);





