function y = chebyshev(n, x)

if n ~= floor(n)
    warning('Input n can be integer only. Rounding down.');
    n = floor(n);
end

if n == 0
    y = 1;
elseif n == 1 
    y = x;
else
    y = 2 * x .* chebyshev(n - 1, x) - chebyshev(n - 2, x);
end;