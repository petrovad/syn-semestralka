clear;
close all;
clc;

fs = 44100;

y = [];

for ii = 1 : 4
    y = [y, snare('c', fs)];
end;


plot(0 : 1/fs : length(y)/fs - 1/fs, y);
soundsc(y, fs);

