function makePianoRoll(notes, stepTime)
    if nargin < 2
        stepTime = 1;
    end

    [PR,t,nn] = piano_roll(notes, 1, stepTime);
    figure;
    imagesc(t,nn,PR);
    axis xy;
    xlabel('time (sec)');
    ylabel('note number');
    c = colorbar('Ticks',[0, 31, 63, 95, 127]);
    c.Label.String = 'Velocity';
end