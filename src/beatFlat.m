function pattern = beatFlat(kicksPerBeat)
% kicksPerBeat of one of 1, 2, 4
switch kicksPerBeat
    case 1
        modul = 4;
    case 2
        modul = 2;
    case 4
        modul = 1;
    otherwise
        pattern = [];
        return;
end;

barNum = 1;
barLen = 16;

patternLen = barNum * barLen;
pattern = zeros(patternLen, 3);

for ii = 1 : patternLen
    if mod(ii - 1, modul) == 0
        pattern(ii, 1) = 127;
    end;
end;