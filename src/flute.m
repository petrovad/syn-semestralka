function y = flute(freq, toneLen, fs, trem)

% definitions
t = 0 : 1/fs : toneLen - (1/fs);

X = [0, 0.2 * toneLen, 0.9 * toneLen, toneLen];
Y = [0, 1, 0.9, 0];
env = interp1(X, Y, t);

harmonics = [1, 2, 3, 4, 5, 6];
amplitudes = [1.0000, 0.2500, 0.0625, 0.0156, 0.0625, 0.0156];

% synthesis
x = sin(2 * pi * freq * t);
y = chebyshev(1, x);
for ii = 2 : length(harmonics)
    y = y + amplitudes(ii) * chebyshev(harmonics(ii), x);
end;

% mastering
y = y .* env;
if trem
    y = tremolo(y, toneLen, fs, 0.25);
end;
y = normalize(y);
