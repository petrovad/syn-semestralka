function y = distort(x, gain, drive, tone)
% gain  � 1 ... 100
% drive - 1 ... 100
% tone  � 
% 
% if gain < 1
%     gain = 1;
% end;
% if drive < 1
%     drive = 1;
% end;

y = gain * x;
a = sin((drive / 100) * pi / 2);
k = 2 * a / (1 - a);
y = (1 + k) * y ./ (1 + k * abs(y));

y = normalize(y);
