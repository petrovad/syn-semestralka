function y = crash(note, fs)

% definitions
toneLen = 2;
t = 0 : 1/fs : toneLen - (1/fs);

freqs = [getFreq(note, 4),
         getFreq(note, 5),
         getFreq(note, 6),
         getFreq(note, 7),
         getFreq(note, 8)];
taus = [0.75, 0.6, 0.45, 0.3, 0.15];
taus = taus * 3;
amplitudes = [1, 0.9, 0.8, 0.7, 0.6];

tau_n = 0.3;
X = [0, 0.05 * toneLen, toneLen];
Y = [0, 1, 1];
env_noise = exp(-t/tau_n) .* interp1(X, Y, t);

reverbname = './res/IMreverbs/Chateau de Logne, Outside.wav';

% synthesis
y = zeros(1, length(t));
for ii = 1 : length(freqs)
    y_n = conv(noisewave(toneLen, fs), square(2 * pi * freqs(ii) * t));
    y_n = y_n(1 : length(t));
    y_n = y_n(end : -1 : 1);
    y = y + amplitudes(ii) * y_n .* exp(-t/taus(ii));
end;

% mastering
y = mix(y, noisewave(toneLen, fs) .* env_noise, 1 / length(freqs), 1);
y = filter_wrap(freqs(1), fs, y, 'high');
y = reverb(y, reverbname);
y = normalize(y);





