function y = timpani(freq, toneLen, fs)

% definitions
t = 0 : 1/fs : toneLen - 1/fs; 

X = [0, 0.01 * toneLen, 0.99 * toneLen, toneLen];
Y = [0, 1, 0.9, 0];
env = exp(-2.8 * t) .* interp1(X, Y, t);

harmonics = [1.0000, 1.5958, 2.1417, 2.3000, 2.6583, 2.9250, 3.1625, 3.5083, 3.6042, 3.6542, 4.0667, 4.2375, 4.6083, 4.8417, 4.9125, 5.1417, 5.4250, 5.5500, 5.9875, 6.1667, 6.2208, 6.5417, 6.7583, 6.8625, 7.3417, 7.4833, 7.9083, 8.0875, 8.6792, 9.2583];
amplitudes = [1.0000, 0.4518, 0.2740, 0.2427, 0.1897, 0.1613, 0.1412, 0.1184, 0.1131, 0.1105, 0.0921, 0.0859, 0.0745, 0.0685, 0.0668, 0.0618, 0.0564, 0.0543, 0.0477, 0.0454, 0.0447, 0.0411, 0.0388, 0.0378, 0.0337, 0.0327, 0.0297, 0.0286, 0.0254, 0.0227];

% synthesis
y = zeros(1, length(t));
for ii = 1 : length(harmonics)
    y = y + amplitudes(ii) * sin(2 * pi * freq * harmonics(ii) * t);
end;

% mastering
y = y .* env;
yr = reverb(y, './res/IMreverbs/Trig Room.wav');
y = mix(y, yr, 1, 0.6);
y = normalize(y);


