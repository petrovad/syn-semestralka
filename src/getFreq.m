function f = getFreq(note, oct)
% Returns the frequency of a note. Nonsensical notes (such as b#) are
% returned as -1. Both English and Czech standards for h/b  are accepted.

a4 = 440;
a = 2^(1/12);

switch note
    case 'c'
        f = a4 * a ^ ((oct - 4) * 12 - 9);
    case 'c#'
        f = a4 * a ^ ((oct - 4) * 12 - 8);
    case 'd'
        f = a4 * a ^ ((oct - 4) * 12 - 7);
	case 'd#'
        f = a4 * a ^ ((oct - 4) * 12 - 6);
    case 'e'
        f = a4 * a ^ ((oct - 4) * 12 - 5); 
    case 'f'
        f = a4 * a ^ ((oct - 4) * 12 - 4);
    case 'f#'
        f = a4 * a ^ ((oct - 4) * 12 - 3);
    case 'g'
        f = a4 * a ^ ((oct - 4) * 12 - 2);
    case 'g#'
        f = a4 * a ^ ((oct - 4) * 12 - 1);
    case 'a'
        f = a4 * a ^ ((oct - 4) * 12);
    case 'a#'
        f = a4 * a ^ ((oct - 4) * 12 + 1);
    case 'h'
        f = a4 * a ^ ((oct - 4) * 12 + 2);
    case 'b'
        f = a4 * a ^ ((oct - 4) * 12 + 2);
    otherwise
        f = -1;
end