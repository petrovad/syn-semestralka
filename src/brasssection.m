function y = brasssection(freq, toneLen, fs)

% synthesis
bras = [frenchhorn(freq, toneLen, fs);
        trumpet(freq, toneLen, fs);
        trumpet(freq * 0.99, toneLen, fs);
        trombone(freq, toneLen, fs);
        tuba(freq / 2, toneLen, fs)];
y = sum(bras);

% mastering
y = normalize(y);