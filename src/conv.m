function y = conv(x, h)

N = length(x) + length(h) - 1;
NFFT = 2.^nextpow2(N);

H = fft(h, NFFT);
X = fft(x, NFFT);
Y = H .* X;

y = real(ifft(Y));
y = y(1 : N);
y = y ./ max(abs(y));