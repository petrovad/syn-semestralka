function y = stringensemble2(freq, toneLen, fs)

% synthesis
strings = [violin(freq, toneLen, fs, true);
           violin(freq * 0.99, toneLen, fs, false);
           viola(freq, toneLen, fs, true);
           cello(freq, toneLen, fs, true);
           contrabass(freq / 2, toneLen, fs, false)];
y = sum(strings);

% mastering
y = normalize(y);