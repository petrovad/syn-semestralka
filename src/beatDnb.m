function pattern = beatDnb()

barNum = 1;
barLen = 16;

patternLen = barNum * barLen;
pattern = zeros(patternLen, 3);

pattern(1, 1) = 127;
pattern(11, 1) = 127;

pattern(5, 2) = 127;
pattern(13, 2) = 127;

for ii = 1 : patternLen
    if mod(ii - 1, 2) == 1
        pattern(ii, 3) = randVelocity();
    end;
end;