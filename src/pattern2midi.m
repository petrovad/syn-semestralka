function notes = pattern2midi(pattern, stepTime)

notes = [];
track = 1;
channel = 10;

for ii = 1 : size(pattern, 1)
    % kick
    if (pattern(ii, 1) > 0)
        newEntry = zeros(1, 6);
        
        newEntry(1, 1) = track;
        newEntry(1, 2) = channel;
        newEntry(1, 3) = midiNotes.kick;
        newEntry(1, 4) = pattern(ii, 1);
        newEntry(1, 5) = (ii - 1) * stepTime; % indexing from 1
        newEntry(1, 6) = (ii - 1) * stepTime; 
        
        notes = [notes; newEntry];
    end;
    
    %snare
    if (pattern(ii, 2) > 0)
        newEntry = zeros(1, 6);
        
        newEntry(1, 1) = track;
        newEntry(1, 2) = channel;        
        newEntry(1, 3) = midiNotes.snare;
        newEntry(1, 4) = pattern(ii, 2);
        newEntry(1, 5) = (ii - 1) * stepTime; % indexing from 1
        newEntry(1, 6) = (ii - 1) * stepTime + stepTime * 2;
       
        notes = [notes; newEntry];
    end;    
    
    % hihat
    if (pattern(ii, 3) > 0)
        newEntry = zeros(1, 6);
        
        newEntry(1, 1) = track;
        newEntry(1, 2) = channel;
        newEntry(1, 3) = midiNotes.closedHat;
        newEntry(1, 4) = pattern(ii, 3);
        newEntry(1, 5) = (ii - 1) * stepTime; % indexing from 1
        newEntry(1, 6) = (ii - 1) * stepTime + stepTime; 
        
        notes = [notes; newEntry];
    end;
end;

notes(:, 7 : 8) = 0;
for ii = 1 : size(notes, 1)
    notes(ii, 9) = notes(ii, 3);
end;