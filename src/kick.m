function y = kick(note, fs)

% definitions
f1 = getFreq(note, 1);
f2 = getFreq(note, 2);
f8 = getFreq(note, 8);

toneLen = 0.11;
t = 0 : 1/fs : toneLen - (1/fs);

% transient
transLen = 0.009 * toneLen;
X = [0, transLen, toneLen];
Y = [1, 0, 0];
env = interp1(X, Y, t); 

transient = noisewave(toneLen, fs);
transient = filter_wrap(f8, fs, transient, 'low');
transient = transient .* env;

% body
Xpitch = [0, 1];
Ypitch = [f2, f1];
freqs = interp1(Xpitch, Ypitch, t);

X = [0, 0.99 * transLen, transLen, 0.25 * toneLen, toneLen];
Y = [0, 0, 1, 0.7, 0];
env = interp1(X, Y, t);

body = sin(2 * pi * freqs .* t) .* env;

% mastering
y = mix(body, transient, 1, 0.5);
yc = compress(y, 0.4, 10000);
dry_wet = linspace(0, 1, length(t));
y = y .* dry_wet + yc .* (1 - dry_wet);
yd = distort(y, 100, 50, 50);
y = mix(y, yd, 1, 0.1);
yr = reverb(y, './res/IMreverbs/Trig Room.wav');
y = mix(y, yr, 1, 0.5);
y = normalize(y);







