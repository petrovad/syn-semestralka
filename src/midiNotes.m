classdef midiNotes
    properties (Constant)
        kick = 36
        snare = 38
        closedHat = 42
        openHat = 46
    end
end