function y = compress(x, threshold, rate)

if (threshold < 0 || threshold > 1 || rate < 0) 
    return;
end;

y = normalize(x);

for i = 1 : length(y)
    if (y(i) > threshold)
        y(i) = threshold + (y(i) - threshold) / rate;
    elseif ((y(i) < -threshold))
        y(i) = -threshold - (y(i) + threshold) / rate;
    end;
end;

y = normalize(y);
