function y = filter_wrap(fc, fs, x, type)

n = 100;
wn = fc /(fs / 2);
a = 1;
b = fir1(n, wn, type);
y = filter(b, a, x);