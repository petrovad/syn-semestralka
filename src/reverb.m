function y = reverb(x, file)

y = [x, zeros(1, length(x))];

a = 1;
b = audioread(file);
b = (b(:, 1) + b(:, 2)) / 2;

y = filter(b, a, y);

% remove tracing zeros
n = 0;
for ii = length(y) : -1 : 1
    if abs(y(ii)) < 3e-3
        n = n + 1;
    else
        break;
    end;
end;

y = y(1, 1 : length(y) - n);

y = normalize(y);