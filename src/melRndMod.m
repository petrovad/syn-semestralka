function y = melRndMod(fs, bpm)

% nosna frekvence [Hz]
a4 = 440;
a = 2^(1/12);
dists = [-35, -38, -36, -29, -43];
fc = a4 * a .^ (dists);

% modulacni frekvence [Hz]
fm = 100 .* rand(6, 1);

Io = [5, 7, 15, 7, 10]; % modulacni index
beats = [1, 0.5, 0.5, 0.5, 1.5];
lengths = 60 / bpm .* beats;

y = [];

for k = 1 : length(fc)
    t = 0 : 1/fs : lengths(k) - 1/fs;
    
    attack = rand;
    sustain = rand;
    if (attack > sustain)
        tmp = attack;
        attack = sustain;
        attack = tmp;
    end;
    if (sustain > 0.99)
        sustain = 0.9;
    end;
    env = interp1([0, attack * lengths(k), sustain * lengths(k), 0.99 * lengths(k), lengths(k)],...
                  [0, 1, 0.7, 0.5, 0], t);
    
              
    mi = Io(k) * env;
    y = [y, env .* sin(2 * pi * fc(k) * t + mi .* sin(2 * pi * fm(k) * t))];       
end;

y = compress(y, 0.7, 10000);

