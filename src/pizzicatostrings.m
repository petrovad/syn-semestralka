function y = pizzicatostrings(freq, toneLen, fs)

y = orchestralharp(freq, toneLen, fs);
y = compress(y, 0.4, 10000);