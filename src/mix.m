function y = mix(x1, x2, x1amount, x2amount)
% TODO make arguments arrays of channels � y = mix(channels, gains)

if length(x1) > length(x2)
    y = x1 * x1amount;
    for ii = 1 : length(x2)
        y(ii) = y(ii) + x2amount * x2(ii);
    end;
else
    y = x2 * x2amount;
    for ii = 1 : length(x1)
        y(ii) = y(ii) + x1amount * x1(ii);
    end;
end;

y = normalize(y);