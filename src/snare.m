function y = snare(note, fs)

f0 = getFreq(note, 0);
f1 = getFreq(note, 3);
f2 = getFreq(note, 4);
f8 = getFreq(note, 8);

toneLen = 0.2;
t = 0 : 1/fs : toneLen - (1/fs);

% transient
transLen = 0.0025;
X = [0, transLen, toneLen];
Y = [1, 0, 0];
env = interp1(X, Y, t); 

transient = noisewave(toneLen, fs);
transient = filter_wrap(f8, fs, transient, 'low');
transient = transient .* env;

% body
bodyLen = 0.03;
Xpitch = [0, 1];
Ypitch = [f2, f1];
freqs = interp1(Xpitch, Ypitch, t);

X = [0, 0.99 * transLen, transLen, 0.8 * bodyLen, bodyLen, toneLen];
Y = [0, 0, 1, 0.9, 0, 0];
env = interp1(X, Y, t);

body = sin(2 * pi * freqs .* t) .* env;
body = mix(distort(body, 100, 50, 50), body, 1, 1);
body = normalize(body);

% tail
X = [0, 0.99 * bodyLen, bodyLen, 0.99 * toneLen, toneLen];
Y = [0, 0, 1, 1, 0];
env = exp(-10 * t) .* interp1(X, Y, t);

g = 0.5;

x = zeros(1, toneLen * fs);
D = round(fs / f0);
x(1 : D) = 2 * rand(1, D) - 1;
    
b = 1;
a = [1, zeros(1, D - 1), -g -g];
    
tail = filter(b, a, x) .* env;
tail = mix(distort(tail, 100, 10, 50), tail, 1, 1);
tail = normalize(tail);

% mastering
y = mix(transient, body, 1, 1);
y = mix(y, tail, 1, 0.75);
y = distort(y, 100, 50, 50);
y = compress(y, 0.5, 100);
y = filter_wrap(f8 * 1.5, fs, y, 'low');
y = reverb(y, './res/IMreverbs/Cement Blocks 1.wav');
y = normalize(y);

