function y = melRnd(tones, len, fs)

lengths = rand(1, tones);
div = sum(lengths) / len;
lengths = lengths / div;

cmaj = 'cdefgah';
freqs = zeros(1, tones);
for ii = 1 : tones
    freqs(ii) = getFreq(cmaj(mod(round(rand * 10), 7) + 1), 1);
end;

y = [];
for ii = 1 : tones
    t = 0 : 1/fs : lengths(ii) - 1/fs;
    
    X = [0, 0.6 * lengths(ii), 0.7 * lengths(ii), 0.8 * lengths(ii), lengths(ii)];
    Y = [0, 1, 0.9, 0.5, 0];
    env = interp1(X, Y, t);
    
    y = [y, square(2 * pi * freqs(ii) * t) .* env];
end;
    
    