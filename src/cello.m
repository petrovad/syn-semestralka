function y = cello(freq, toneLen, fs, trem)

% definitions
t = 0 : 1/fs : toneLen - (1/fs);

X = [0, 0.3 * toneLen, 0.9 * toneLen, toneLen];
Y = [0, 1, 1, 0];
env = interp1(X, Y, t);

formants = [990, 2000];
bws = [450, 560];

% synthesis
y = sawtooth(2 * pi * freq * t);

for k = 1 : length(formants)                               
    R = 1 - bws(k) * pi / fs;
    pp = R * exp(1i * 2 * pi * formants(k) / fs);
    pn = R * exp(-1i * 2 * pi * formants(k) / fs);
    [b, a] = zp2tf(0, [pp, pn], 1);
    y = filter(b, a, y);
end

% mastering
y = filter_wrap(freq, fs, y, 'high');
y = y .* env;
if trem
    y = tremolo(y, toneLen, fs, 0.25);
end;
y = normalize(y);