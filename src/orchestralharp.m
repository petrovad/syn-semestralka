function y = orchestralharp(freq, toneLen, fs)

% definitions
t = 0 : 1/fs : toneLen - (1/fs);

X = [0, 0.01 * toneLen, 0.99 * toneLen, toneLen];
Y = [0, 1, 1, 0];
env = exp(-1 * t) .* interp1(X, Y, t);

harmonics = [1, 2, 3, 4, 5];
amplitudes = [1, 1, 0.5, 0.3, 0.1];
taus = [0.7, 0.3, 0.1, 0.1, 0.1];

x = sin(2 * pi * freq * t);
y = zeros(1, length(t));
for ii = 1 : length(harmonics)
    y = y + amplitudes(ii) * compress(chebyshev(harmonics(ii), x), 0.7, 1000) .* exp(-t/taus(ii)) .* env;
end;

% mastering
y = normalize(y);