function y = tremolo(x, toneLen, fs, strength)
% strength 0 ... 1 (0 no tremolo, 1 hard tremolo)

t = 0 : 1/fs : toneLen - (1/fs);

vibes = floor(toneLen + 0.5) * 8;
T = toneLen / vibes;
f = 1 / T;
strengths = interp1([0, toneLen], [strength / 2, strength / 8], t);
env = strengths .* cos(2 * pi * f * t);
env = env + (1 - strengths);

y = x .* env;
