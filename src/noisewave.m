function y = noisewave(toneLen, fs)

y = rand(1, toneLen * fs) * 2 - 1;
