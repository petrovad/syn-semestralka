function y = trumpet(freq, toneLen, fs, trem)

% definitions
t = 0 : 1/fs : toneLen - 1/fs; 

harmonics = [1, 2, 3, 4, 5, 6, 7, 8, 9];
amplitudes = [1.0000, 1.8222, 1.4444, 0.5556, 0.2222, 0.0889, 0.0667, 0.0444, 0.0222];
X = [0, 0.0071, 0.9875, 1;
     0, 0.0286, 0.9750, 1;
     0, 0.0643, 0.9625, 1;
     0, 0.1143, 0.9500, 1;
     0, 0.1786, 0.9375, 1;
     0, 0.2571, 0.9250, 1;
     0, 0.3500, 0.9125, 1;
     0, 0.4571, 0.9000, 1;
     0, 0.5786, 0.8875, 1] * toneLen;
Y = [0, 1, 0.8, 0];

% synthesis
y = zeros(1, length(t));
for ii = 1 : length(harmonics)
    env = interp1(X(ii, :), Y, t);
    y = y + amplitudes(ii) * sin(2 * pi * freq * harmonics(ii) * t) .* env;
end;

% mastering
if trem
    y = tremolo(y, toneLen, fs, 0.3);
end;
y = normalize(y);


