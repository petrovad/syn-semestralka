function y = normalize(x)

y = x - mean(x);
y = y ./ max(abs(y));