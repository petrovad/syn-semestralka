%% OBSLUZNY SKRIPT PRO CTENI MIDI
% MIDI soubory nahrajte prosim do podadresare midi. Podadresar ./src
% obsahuje doplnkove funkce pro cteni MIDI souboru. O cteni MIDI souboru se
% tedy jiz nemusite vubec starat. Vasim ukolem je navrhnout syntezu 
% jednotlivych not. Jednotlive noty se syntetizuji ve funkci synth.m, jejiz 
% kostru naleznete v korenovem adresari. Sve reseni syntezy prosim doplnte 
% do teto kostry. Napovedu a pokyny naleznete v hlavicce funkce midi2synth
% a synth. Blizsi informace o cislech jednotlivych typu nastroju naleznete 
% v dokumentu MIDI_instrument_table.pdf.
%
% Upravene funkce pro cteni MIDI souboru vychazi z puvodniho navrhu Ken 
% Schutte (kenschutte@gmail.com) a jsou dostupne vcetne dokumentace na 
% http://kenschutte.com/midi podle GPL.

%% Polyfonni synteza - cisla nastroju podle midi-souboru (CTRL + ENTER)
addpath .\src .\res .\midi_toolbox % Pridani cesty ke skriptum a souborum MIDI
notes = readmidi('Vltava.mid');
[y, fs] = midi2synth(notes, 48e3);
y = normalize(y);
audiowrite('.\audio\Vltava.wav', y, fs);
rmpath .\src .\res .\midi_toolbox

%% Stupnice
% a) Vypoctete si vektor frekvenci durove stupnice v rozsahu odpovidajicim
%    nastroji.
% b) Zvolte si rozumnou dobu trvani tonu.
% c) Syntetizujte jednotlive noty pomoci funkce synth.m
% d) Vysledny audio zaznam stupnice zapiste do wav souboru (nezapomente
%    signal normalizovat)

addpath .\src;

% definitions
fs = 44100;
toneLen = 0.8;
instr = 'stringensemble2';

cmaj = 'gahcdef';
octs = 3;
oct = 3;

% synthesis
% standard instrument table
y = [];

for ii = 0 : 7 * octs - 1
    idx = mod(ii, 7) + 1;
    if ii > 0 && cmaj(idx) == 'c'
        oct = oct + 1;
    end;
    freq = getFreq(cmaj(idx), oct);
    sound = synth(freq, toneLen, 1, fs, eval(strcat('midiInstr.', instr)));
    y = [y, sound];
end;

% percussion instrument table (crash)
% y = [];
% for ii = 1 : length(cmaj)
%     y = [y, crash(cmaj(ii), fs)];
% end;

% output
audiowrite(strcat('./audio/', instr, '.wav'), y, fs);

%% Volna uloha
addpath .\src .\res .\midi_toolbox;

% definitions
fs = 44100;
bpm = 87;

p_kick = 1/3;
p_snare = 1/8;
p_hat = 2/5;

g_kick = 1;
g_snare = 0.7;
g_hat = 0.2;

% synthesis
% beats
pattern = [beatFlat(1);
           beatFlat(2);];
for ii = 1 : 4
    pattern = [pattern;
               beatDnb();
               beatDnb();
               beatDnb();
               beatRnd(p_kick, p_snare, p_hat)];
end;
pattern = [pattern;
           beatFlat(1);
           beatFlat(1);
           beatFlat(1);
           beatFlat(1);
           beatFlat(1);
           beatFlat(1);
           beatFlat(2);
           beatFlat(2);
           beatFlat(4);
           beatFlat(4)];
break1 = beatRnd(p_kick, 2 * p_snare, p_hat);
break2 = beatRnd(p_kick, 2 * p_snare, p_hat);
for ii = 1 : 2
    pattern = [pattern;
               break1;
               break1;
               break1;
               beatDnb();
               break2;
               break2;
               break2;
               beatDnb();];
end;

patternLen = size(pattern, 1);
patternTime = patternLen / 4 * 60 / bpm;
stepTime = patternTime / patternLen;

notes = pattern2midi(pattern, stepTime);

endtime = max(notes(:, 6));
beats = zeros(1, (ceil(endtime + 1) * fs));

for i = 1 : size(notes, 1)
  f = midi2freq(notes(i, 3));
  dur = notes(i, 6) - notes(i, 5);
  amp = notes(i, 4) / 127;
  synthtype = notes(i , 3);
  channel = notes(i, 2);
  yt = synth(f, dur, amp, fs, synthtype, channel);
    
  switch synthtype
      case 36
          yt = yt * g_kick;
      case 38
          yt = yt * g_snare;
      otherwise
          yt = yt * g_hat;
  end;
      
  n1 = floor(notes(i, 5) * fs) + 1;
  N = length(yt);  

  n2 = n1 + N - 1;

  % ensure yt is [1,N]:
  beats(n1 : n2) = beats(n1 : n2) + reshape(yt, 1, []);
end

% bass
beatLen = 60 / bpm;
barLen = 4 * beatLen;

bass = zeros(1, length(beats));

melody = melRnd(5, barLen, fs);
filterfreqs = [200, 800, 1600];
melody1 = filter_wrap(filterfreqs(1), fs, melody, 'low');
melody2 = filter_wrap(filterfreqs(2), fs, melody, 'low');
melody3 = filter_wrap(filterfreqs(3), fs, melody, 'low');

offset = 2;
for ii = 1 : 4
    pos = ((ii - 1) * 4 + offset) * round(barLen * fs);
    bass(pos + 1 : pos + 3 * length(melody)) = [melody1, melody2, melody3];
end;

offset = 18;
for ii = 1 : 6
    pos = ((ii - 1) + offset) * round(barLen * fs);
    bass(pos + 1 : pos + length(melody)) = melody1;
end;

halfMelLen = round(length(melody) / 2);

offset = 24;
for ii = 1 : 4
    pos = ((ii - 1) + offset * 2) * round(barLen / 2 * fs);
    bass(pos + 1 : pos + halfMelLen) = melody2(1 : halfMelLen);
end;

offset = 26;
for ii = 1 : 4
    pos = ((ii - 1) + offset * 2) * round(barLen / 2 * fs);
    bass(pos + 1 : pos + halfMelLen) = melody3(1 : halfMelLen);
end;


halfmelody1 = [melody1(1 : halfMelLen), zeros(1, halfMelLen)];
halfmelody2 = [melody2(1 : halfMelLen), zeros(1, halfMelLen)];
halfmelody3 = [melody3(1 : halfMelLen), zeros(1, halfMelLen)];
halfmelody = [halfmelody1, halfmelody2, halfmelody3];

offset = 28;
for ii = 1 : 4
    pos = ((ii - 1) * 4 + offset) * round(barLen * fs);
    bass(pos + 1 : pos + length(halfmelody)) = halfmelody;
end;

modmel = melRndMod(fs, bpm);

offset = 28;
for ii = 1 : 4
    pos = ((ii - 1) * 4 + offset + 3) * round(barLen * fs);
    bass(pos + 1 : pos + length(modmel)) = melRndMod(fs, bpm);
end;

y = mix(beats, bass, 1, 0.4);
y = compress(y, 0.6, 100);
y = normalize(y);
audiowrite(strcat('./audio/footwork.wav'), y, fs);








