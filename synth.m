function y = synth(freq, toneLen, amp, fs, synthtype, channel)
% y = synth(midinote, toneLen, amp, fs, synthtype, channel)
% Tato funkce realizuje syntezu jedine noty. Pro realizaci syntezy
% potrebujete znat:
%
% freq - frekvence noty
% toneLen - trvani noty
% amp - amplituda v rozsahu [0, 1]
% fs - vzorkovaci frekvenci
% synthtype - cislo nastroje podle MIDI (viz. MIDI_instrument_table.pdf)
% channel (nepovinny udaj) - cislo kanalu (kanal 10 je vyhrazen perkusim a 
% ridi se vlastni tabulkou nastroju viz. MIDI_intrument_table.pdf)
% 
% Tato kostra pochazi z puvodni funkce synth.m navrzene panem Ken Schutte 
% (kenschutte@gmail.com). Puvodni funkce synth a funkce pro cteni MIDI jsou 
% dostupne vcetne dokumentace na http://kenschutte.com/midi podle GPL.
%
%

if nargin==5
    channel=1;
end
y = [];
if channel == 10 % Podle tabulky perkusnich nastroju (kanal 10)
    switch synthtype 
        case 36
            y = kick('c', fs)';
        case 38
            y = snare('g', fs)';
        case 42
            y = hihat('c', fs, false)';
        case 46
            y = hihat('c', fs, true)';
        case 49
            y = crash('c', fs)';
    end;    
else % Podle standardni tabulky nastroju
    switch synthtype 
        case 41
            y = violin(freq, toneLen, fs, true);
        case 42
            y = viola(freq, toneLen, fs, true);
        case 43
            y = cello(freq, toneLen, fs, true);
        case 44
            y = contrabass(freq, toneLen, fs, true);
        case 46
            y = pizzicatostrings(freq, toneLen, fs);
        case 47
            y = orchestralharp(freq, toneLen, fs);
        case 48
            y = timpani(freq, toneLen, fs);
        case 49
            y = stringensemble1(freq, toneLen, fs);
        case 50
            y = stringensemble2(freq, toneLen, fs);    
        case 57
            y = trumpet(freq, toneLen, fs, true);
        case 58
            y = trombone(freq, toneLen, fs);
        case 59
            y = tuba(freq, toneLen, fs);
        case 61
            y = frenchhorn(freq, toneLen, fs);
        case 62
            y = brasssection(freq, toneLen, fs);
        case 69
            y = oboe(freq, toneLen, fs, true);
        case 71
            y = bassoon(freq, toneLen, fs);
        case 72
            y = clarinet(freq, toneLen, fs, true);
        case 74
            y = flute(freq, toneLen, fs, true);
        otherwise
%             t = 0 : 1/fs : toneLen - (1/fs);
%             y = square(2 * pi * freq * t);
    end
    y = reverb(y, './res/IMreverbs/Chateau de Logne, Outside.wav');
end

y = y .* amp;



